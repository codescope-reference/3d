# Some 3D stuff

# Stanford Bunny
https://graphics.stanford.edu/data/3Dscanrep/


# Book
https://www.cs.kent.edu/~dragan/CG/CG-Book.pdf


# Examples so far

## Bunny with 3D points
![Bunny with 3D points](bunny_points.png)

## Convex Hull of random points
![Convex Hull of random points](hull.png)
