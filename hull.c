#include "raylib.h"
#include "camera_fix.c"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "rlgl.h"

#define da_append(da, item) \
  if ((da)->count >= (da)->capacity) {  \
    if ((da)->capacity == 0) { \
      (da)->capacity = 1; \
    } \
    (da)->capacity *= 2;  \
    (da)->items = realloc((da)->items, (da)->capacity * sizeof(*(da)->items));  \
    assert((da)->items != NULL); \
  }  \
  (da)->items[(da)->count++] = item;  \


#define da_remove_at(da, index) \
  for (int i = index; i < (da)->count - 1; i++) {  \
    (da)->items[i] = (da)->items[i + 1];  \
  }  \
  (da)->count--;  \

typedef struct PointsDA {
  Vector3 *items;
  int count;
  int capacity;
} PointsDA;


typedef struct Tri {
  Vector3 a;
  Vector3 b;
  Vector3 c;
} Tri;

typedef struct TriDA {
  Tri *items;
  int count;
  int capacity;
} TriDA;

void add_triangle(TriDA *tris, PointsDA *points, int start_index) {
  assert(start_index + 3 < points->count);

  Tri new_tri = (Tri) {
    points->items[start_index + 0],
    points->items[start_index + 1],
    points->items[start_index + 2],
  };

  da_append(tris, new_tri);

  for (int i = 0; i < 3; i++) {
    da_remove_at(points, start_index);
  }
}

bool is_existing_triangle(TriDA *hull, Vector3 p1, Vector3 p2, Vector3 p3) {
  float eps = 0.0001f;
  for (int i = 0; i < hull->count; i++) {
    Tri tri = hull->items[i];

    bool is_tri = false;
    // 1
    bool p1_e_a = Vector3DistanceSqr(p1, tri.a) < eps;
    bool p2_e_b = Vector3DistanceSqr(p2, tri.b) < eps;
    bool p3_e_c = Vector3DistanceSqr(p3, tri.c) < eps;
    is_tri      = (p1_e_a && p2_e_b && p3_e_c);
    if (is_tri) return true;

    // 2
    p1_e_a      = Vector3DistanceSqr(p1, tri.a) < eps;
    bool p2_e_c = Vector3DistanceSqr(p2, tri.c) < eps;
    bool p3_e_b = Vector3DistanceSqr(p3, tri.b) < eps;
    is_tri      = (p1_e_a && p2_e_c && p3_e_b);
    if (is_tri) return true;

    // 3
    bool p1_e_b = Vector3DistanceSqr(p1, tri.b) < eps;
    bool p2_e_a = Vector3DistanceSqr(p2, tri.a) < eps;
    p3_e_c      = Vector3DistanceSqr(p3, tri.c) < eps;
    is_tri      = (p1_e_b && p2_e_a && p3_e_c);
    if (is_tri) return true;

    // 4
    p1_e_b      = Vector3DistanceSqr(p1, tri.b) < eps;
    p2_e_c      = Vector3DistanceSqr(p2, tri.c) < eps;
    bool p3_e_a = Vector3DistanceSqr(p3, tri.a) < eps;
    is_tri      = (p1_e_b && p2_e_c && p3_e_a);
    if (is_tri) return true;

    // 5
    bool p1_e_c = Vector3DistanceSqr(p1, tri.c) < eps;
    p2_e_a      = Vector3DistanceSqr(p2, tri.a) < eps;
    p3_e_b      = Vector3DistanceSqr(p3, tri.b) < eps;
    is_tri      = (p1_e_c && p2_e_a && p3_e_b);
    if (is_tri) return true;

    // 6
    p1_e_c      = Vector3DistanceSqr(p1, tri.c) < eps;
    p2_e_b      = Vector3DistanceSqr(p2, tri.b) < eps;
    p3_e_a      = Vector3DistanceSqr(p3, tri.a) < eps;
    is_tri      = (p1_e_c && p2_e_b && p3_e_a);
    if (is_tri) return true;
  }

  return false;
}


bool has_edge(Tri tri, Vector3 p1, Vector3 p2) {
  float eps = 0.00001f;
  bool is_first_edge = (
    Vector3DistanceSqr(p1, tri.a) < eps && Vector3DistanceSqr(p2, tri.b) < eps
    ||
    Vector3DistanceSqr(p1, tri.b) < eps && Vector3DistanceSqr(p2, tri.a) < eps
  );
  if (is_first_edge) return true;

  bool is_second_edge = (
    Vector3DistanceSqr(p1, tri.b) < eps && Vector3DistanceSqr(p2, tri.c) < eps
    ||
    Vector3DistanceSqr(p1, tri.c) < eps && Vector3DistanceSqr(p2, tri.b) < eps
  );
  if (is_second_edge) return true;

  bool is_third_edge = (
    Vector3DistanceSqr(p1, tri.c) < eps && Vector3DistanceSqr(p2, tri.a) < eps
    ||
    Vector3DistanceSqr(p1, tri.a) < eps && Vector3DistanceSqr(p2, tri.c) < eps
  );
  if (is_third_edge) return true;


  return false;
}


void expand_convex_hull(TriDA *hull, PointsDA *points, int tri_index, int edge_index) {
  Tri tri = hull->items[tri_index];

  Vector3 p1, p2, p3;
  if (edge_index == 0) {
    p1 = tri.a;
    p2 = tri.b;
    p3 = tri.c;
  } else if (edge_index == 1) {
    p1 = tri.b;
    p2 = tri.c;
    p3 = tri.a;
  } else {
    p1 = tri.c;
    p2 = tri.a;
    p3 = tri.b;
  }

  for (int i = 0; i < hull->count; i++) {
    if (i == tri_index) continue;

    bool edge_exists = has_edge(hull->items[i], p1, p2);
    if (edge_exists) return;
  }

  Vector3 normal = Vector3CrossProduct(
    Vector3Subtract(p1, p3),
    Vector3Subtract(p2, p3)
  );

  float min_cos_angle = 2.0f;
  int min_index = -1;
  for (int i = 0; i < points->count; i++) {
    Vector3 new_pt = points->items[i];
    bool already_exists;
    for (int j = 0; j < hull->count; j++) {
      already_exists = is_existing_triangle(hull, p1, p2, new_pt);
      if (already_exists) break;
    }
    if (already_exists) continue;



    Vector3 new_normal = Vector3CrossProduct(
      Vector3Subtract(p2, new_pt),
      Vector3Subtract(p1, new_pt)
    );

    float cos_angle = -Vector3DotProduct(normal, new_normal) / (Vector3Length(normal) * Vector3Length(new_normal));

    if (cos_angle < min_cos_angle) {
      min_cos_angle = cos_angle;
      min_index = i;
    }
  }
  assert(min_index != -1);
  // printf("-------------------------\n");
  // printf("Existing triangle:\n");
  // printf("p1: %0.4f %0.4f %0.4f\n", p1.x, p1.y, p1.z);
  // printf("p2: %0.4f %0.4f %0.4f\n", p2.x, p2.y, p2.z);
  // printf("p3: %0.4f %0.4f %0.4f\n", p3.x, p3.y, p3.z);
  // printf("New pt:\n");
  // printf("new_pt: %0.4f %0.4f %0.4f\n", points->items[min_index].x, points->items[min_index].y, points->items[min_index].z);

  Tri new_tri = (Tri) {
    points->items[min_index],
    p2,
    p1,
  };
  da_append(hull, new_tri);
  // da_remove_at(points, min_index);
}

int main(void) {
  InitWindow(1000, 1000, "Convex Hull Prototype");
  SetTargetFPS(30);
  DisableCursor();

  Camera camera = {0};
  camera.position = (Vector3) {0.0f, 00.0f, 10.0f};
  camera.target = (Vector3) {0.0f, 0.0f, 0.0f};
  camera.up = (Vector3) {0.0f, 1.0f, 0.0f};
  camera.fovy = 45.0f;
  camera.projection = CAMERA_PERSPECTIVE;


  PointsDA points = {0};
  float spacing = 0.1f; 
  Vector3 p1 = (Vector3) {spacing, 0.0f, 0.0f};
  Vector3 p2 = (Vector3) {-spacing, 0.0f, 0.0f};
  Vector3 p3 = (Vector3) {0.0f, spacing,0.0f};
  Vector3 p4 = (Vector3) {0.0f, 0.0f, spacing};
  da_append(&points, p1);
  da_append(&points, p2);
  da_append(&points, p3);
  da_append(&points, p4);
  for (int i = 0; i < 10; i++) {
    Vector3 pt = (Vector3) {
      (float)GetRandomValue(-1000, 1000) / 1000.0f,
      (float)GetRandomValue(-1000, 1000) / 1000.0f,
      (float)GetRandomValue(200, 1000) / 1000.0f,
    };
    da_append(&points, pt);
  }

  TriDA tris = {0};

  add_triangle(&tris, &points, 0);


  float radius = 0.01f;
  int edge_index = 0;
  int tri_index = 0;
  while (!WindowShouldClose()) {
    BeginDrawing();
    ClearBackground(ColorFromHSV(0, 0, 0.2));
    DrawFPS(10, 10);


    BeginMode3D(camera);
    rlDisableBackfaceCulling();


    for (int i = 0; i < points.count; i++) {
      DrawSphere(points.items[i], radius, RED);
    }

    for (int i = 0; i < tris.count; i++) {
      Tri tri = tris.items[i];

      Color color = BLUE;
      if (i == tris.count - 1) color = WHITE;
      if (i == tri_index) color = RED;
      DrawTriangle3D(tri.a, tri.b, tri.c, color);
      DrawLine3D(tri.a, tri.b, GREEN);
      DrawLine3D(tri.b, tri.c, GREEN);
      DrawLine3D(tri.c, tri.a, GREEN);

      // Vector3 normal = Vector3CrossProduct(
        // Vector3Subtract(tri.b, tri.a),
        // Vector3Subtract(tri.c, tri.a)
      // );
      // Vector3 center_point = (Vector3) {
        // (tri.a.x + tri.b.x + tri.c.x) / 3.0,
        // (tri.a.y + tri.b.y + tri.c.y) / 3.0,
        // (tri.a.z + tri.b.z + tri.c.z) / 3.0,
      // };
      // DrawLine3D(center_point, Vector3Add(center_point, Vector3Scale(normal, 10.0f)), RED);
    }


    EndMode3D();


    EndDrawing();

    update_camera(&camera, CAMERA_FREE);
    if (IsKeyReleased(KEY_Z)) {
      camera.target = (Vector3) {0.0f, 0.0f, 0.0f};
    }
    if (IsKeyReleased(KEY_T)) {
      if (tri_index < tris.count) {
        if (edge_index < 3) {
          expand_convex_hull(&tris, &points, tri_index, edge_index);
          edge_index++;
        } else {
          printf("Incrementing tri index\n");
          edge_index = 0;
          tri_index++;
        }
      } else {
        printf("Done, no more free edges\n");
      }
    }
    if (IsKeyReleased(KEY_R)) {
      while (tri_index < tris.count) {
        if (edge_index < 3) {
          expand_convex_hull(&tris, &points, tri_index, edge_index);
          edge_index++;
        } else {
          edge_index = 0;
          tri_index++;
        }
      } 
    }
    if (IsKeyReleased(KEY_P)) {
      TakeScreenshot("hull.png");
    }
  }


  CloseWindow();
  return 0;
}
