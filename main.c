#include "raylib.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include "raymath.h"
#include "rcamera.h"
#include "rlgl.h"
#include <time.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"


#define CAMERA_ORBITAL_SPEED 1.0f
#define CAMERA_ROTATION_SPEED 0.01f
#define CAMERA_PAN_SPEED 0.01f
#define CAMERA_MOUSE_MOVE_SENSITIVITY 0.001f
#define CAMERA_MOVE_SPEED 0.5f

typedef struct Vertex {
  float x, y, z;
} Vertex;

typedef struct PlyData {
  unsigned long num_vertices;
  Vector3 *vertices;
} PlyData;


void free_plydata(PlyData data) {
  free(data.vertices);
}


typedef struct SV {
  char *start;
  int length;
} SV;


SV sv_from_string(char *str) {
  SV sv;
  sv.start = str;
  sv.length = strlen(str);
  return sv;
}


SV sv_chop_line(SV *sv) {
  SV line = (SV) {sv->start, 0};
  while (sv->length > 0 && *sv->start != '\n') {
    sv->start++;
    sv->length--;
    line.length++;
  }
  sv->start++;
  sv->length--;

  return line;
}

SV sv_chop_word(SV *sv) {
  SV word = (SV) {sv->start, 0};
  while (sv->length > 0 && *sv->start != ' ' && *sv->start != '\n') {
    sv->start++;
    sv->length--;
    word.length++;
  }
  sv->start++;
  sv->length--;

  return word;
}


bool sv_equals_string(SV sv, char *str) {
  if (sv.length != strlen(str)) {
    return false;
  }

  for (int i = 0; i < sv.length; i++) {
    if (sv.start[i] != str[i]) {
      return false;
    }
  }

  return true;
}


bool sv_startswith_string(SV sv, char *str) {
  if (sv.length < strlen(str)) {
    return false;
  }

  for (int i = 0; i < strlen(str); i++) {
    if (sv.start[i] != str[i]) {
      return false;
    }
  }

  return true;
}


PlyData read_ply_file(const char *filename) {
  // Read all the file contents
  // --------------------------
  FILE *file = fopen(filename, "r");
  unsigned long file_size = 0;

  fseek(file, 0, SEEK_END); // Go to the end of the file
  file_size = ftell(file); // Get the current position
  fseek(file, 0, SEEK_SET); // Go back to the beginning


  char *file_contents = (char *)malloc(file_size + 1);
  fread(file_contents, 1, file_size, file);
  file_contents[file_size] = '\0';

  fclose(file);

  // Parse the file
  // --------------
  SV contents = sv_from_string(file_contents);

  SV first_line = sv_chop_line(&contents);
  assert(sv_equals_string(first_line, "ply"));



  // Skip until we get to element vertex N
  bool found = false;
  SV line;
  while (!found) {
    line = sv_chop_line(&contents);
    if (sv_startswith_string(line, "element vertex")) {
      found = true;
    }
  }
  unsigned long n_vertices = atoi(line.start + strlen("element vertex "));
  // n_vertices = n_vertices / 300;

  // Skip until we get to end_header
  found = false;
  while (!found) {
    line = sv_chop_line(&contents);
    if (sv_equals_string(line, "end_header")) {
      found = true;
    }
  }


  // Read the vertices
  Vector3 *vertices = calloc(n_vertices, sizeof(Vector3)); // malloc(n_vertices * sizeof(Vector3));

  for (int i = 0; i < n_vertices; i++) {
    line = sv_chop_line(&contents);
    char *line_start = line.start;
    int line_length = line.length;
    SV word = sv_chop_word(&line);
    vertices[i].x = atof(word.start);
    if (fabs(vertices[i].x) > 2.0) {
      printf("Failed at line %.*s\n", line_length, line_start);
      break;
    }
    assert(fabs(vertices[i].x) < 2.0);

    word = sv_chop_word(&line);
    vertices[i].y = atof(word.start);
    if (fabs(vertices[i].y) > 2.0) {
      printf("Failed at line %.*s\n", line_length, line_start);
      break;
    }
    assert(fabs(vertices[i].y) < 2.0);

    word = sv_chop_word(&line);
    vertices[i].z = atof(word.start);
    if (fabs(vertices[i].z) > 2.0) {
      printf("Failed at line %.*s\n", line_length, line_start);
      break;
    }
    assert(fabs(vertices[i].z) < 2.0);
  }


  return (PlyData) {n_vertices, vertices};
}

void update_camera(Camera *camera, int mode)
{
  Vector2 mousePositionDelta = GetMouseDelta();
  // mousePositionDelta.x = 0.0;
  // mousePositionDelta.y = 0.0;

  bool moveInWorldPlane = ((mode == CAMERA_FIRST_PERSON) || (mode == CAMERA_THIRD_PERSON));
  bool rotateAroundTarget = ((mode == CAMERA_THIRD_PERSON) || (mode == CAMERA_ORBITAL));
  bool lockView = ((mode == CAMERA_FIRST_PERSON) || (mode == CAMERA_THIRD_PERSON) || (mode == CAMERA_ORBITAL));
  bool rotateUp = false;

  if (mode == CAMERA_ORBITAL)
  {
    // Orbital can just orbit
    Matrix rotation = MatrixRotate(GetCameraUp(camera), CAMERA_ORBITAL_SPEED*GetFrameTime());
    Vector3 view = Vector3Subtract(camera->position, camera->target);
    view = Vector3Transform(view, rotation);
    camera->position = Vector3Add(camera->target, view);
  }
  else
{
    // Camera rotation
    if (IsKeyDown(KEY_DOWN)) CameraPitch(camera, -CAMERA_ROTATION_SPEED, lockView, rotateAroundTarget, rotateUp);
    if (IsKeyDown(KEY_UP)) CameraPitch(camera, CAMERA_ROTATION_SPEED, lockView, rotateAroundTarget, rotateUp);
    if (IsKeyDown(KEY_RIGHT)) CameraYaw(camera, -CAMERA_ROTATION_SPEED, rotateAroundTarget);
    if (IsKeyDown(KEY_LEFT)) CameraYaw(camera, CAMERA_ROTATION_SPEED, rotateAroundTarget);
    if (IsKeyDown(KEY_Q)) CameraRoll(camera, -CAMERA_ROTATION_SPEED);
    if (IsKeyDown(KEY_E)) CameraRoll(camera, CAMERA_ROTATION_SPEED);

    // Camera movement
    if (!IsGamepadAvailable(0))
    {
      // Camera pan (for CAMERA_FREE)
      if ((mode == CAMERA_FREE) && (IsMouseButtonDown(MOUSE_BUTTON_MIDDLE)))
      {
        const Vector2 mouseDelta = GetMouseDelta();
        if (mouseDelta.x > 0.0f) CameraMoveRight(camera, CAMERA_PAN_SPEED, moveInWorldPlane);
        if (mouseDelta.x < 0.0f) CameraMoveRight(camera, -CAMERA_PAN_SPEED, moveInWorldPlane);
        if (mouseDelta.y > 0.0f) CameraMoveUp(camera, -CAMERA_PAN_SPEED);
        if (mouseDelta.y < 0.0f) CameraMoveUp(camera, CAMERA_PAN_SPEED);
      }
      else
    {
        // Mouse support
        CameraYaw(camera, -mousePositionDelta.x*CAMERA_MOUSE_MOVE_SENSITIVITY, rotateAroundTarget);
        CameraPitch(camera, -mousePositionDelta.y*CAMERA_MOUSE_MOVE_SENSITIVITY, lockView, rotateAroundTarget, rotateUp);
      }

      // Keyboard support
      if (IsKeyDown(KEY_W)) CameraMoveForward(camera, CAMERA_MOVE_SPEED, moveInWorldPlane);
      if (IsKeyDown(KEY_A)) CameraMoveRight(camera, -CAMERA_MOVE_SPEED, moveInWorldPlane);
      if (IsKeyDown(KEY_S)) CameraMoveForward(camera, -CAMERA_MOVE_SPEED, moveInWorldPlane);
      if (IsKeyDown(KEY_D)) CameraMoveRight(camera, CAMERA_MOVE_SPEED, moveInWorldPlane);
    }
    else
  {
      // Gamepad controller support
      CameraYaw(camera, -(GetGamepadAxisMovement(0, GAMEPAD_AXIS_RIGHT_X) * 2)*CAMERA_MOUSE_MOVE_SENSITIVITY, rotateAroundTarget);
      CameraPitch(camera, -(GetGamepadAxisMovement(0, GAMEPAD_AXIS_RIGHT_Y) * 2)*CAMERA_MOUSE_MOVE_SENSITIVITY, lockView, rotateAroundTarget, rotateUp);

      if (GetGamepadAxisMovement(0, GAMEPAD_AXIS_LEFT_Y) <= -0.25f) CameraMoveForward(camera, CAMERA_MOVE_SPEED, moveInWorldPlane);
      if (GetGamepadAxisMovement(0, GAMEPAD_AXIS_LEFT_X) <= -0.25f) CameraMoveRight(camera, -CAMERA_MOVE_SPEED, moveInWorldPlane);
      if (GetGamepadAxisMovement(0, GAMEPAD_AXIS_LEFT_Y) >= 0.25f) CameraMoveForward(camera, -CAMERA_MOVE_SPEED, moveInWorldPlane);
      if (GetGamepadAxisMovement(0, GAMEPAD_AXIS_LEFT_X) >= 0.25f) CameraMoveRight(camera, CAMERA_MOVE_SPEED, moveInWorldPlane);
    }

    if (mode == CAMERA_FREE)
    {
      if (IsKeyDown(KEY_SPACE)) CameraMoveUp(camera, CAMERA_MOVE_SPEED);
      if (IsKeyDown(KEY_LEFT_CONTROL)) CameraMoveUp(camera, -CAMERA_MOVE_SPEED);
    }
  }

  if ((mode == CAMERA_THIRD_PERSON) || (mode == CAMERA_ORBITAL) || (mode == CAMERA_FREE))
  {
    // Zoom target distance
    CameraMoveToTarget(camera, -GetMouseWheelMove());
    if (IsKeyPressed(KEY_KP_SUBTRACT)) CameraMoveToTarget(camera, 2.0f);
    if (IsKeyPressed(KEY_KP_ADD)) CameraMoveToTarget(camera, -2.0f);
  }
}


Mesh create_mesh_from_ply_data(PlyData data) {
  Mesh mesh = {0};
  mesh.vertexCount = data.num_vertices * 4 * 3;
  mesh.triangleCount = data.num_vertices * 4;


  mesh.vertices = malloc(mesh.vertexCount * 3 * sizeof(float));

  float size = 0.0005f;
  size_t index = 0;
  for (int i = 0; i < data.num_vertices; i++) {
    assert(index < mesh.vertexCount * 3);
    Vector3 v = data.vertices[i];
    Vector3 v1 = {v.x + size, v.y, v.z};
    Vector3 v2 = {v.x - size, v.y, v.z};
    Vector3 v3 = {v.x, v.y + size, v.z};
    Vector3 v4 = {v.x, v.y, v.z - size};


    // First triangle
    mesh.vertices[index++] = v1.x;
    mesh.vertices[index++] = v1.y;
    mesh.vertices[index++] = v1.z;

    mesh.vertices[index++] = v2.x;
    mesh.vertices[index++] = v2.y;
    mesh.vertices[index++] = v2.z;

    mesh.vertices[index++] = v3.x;
    mesh.vertices[index++] = v3.y;
    mesh.vertices[index++] = v3.z;


    // Second triangle
    mesh.vertices[index++] = v1.x;
    mesh.vertices[index++] = v1.y;
    mesh.vertices[index++] = v1.z;

    mesh.vertices[index++] = v2.x;
    mesh.vertices[index++] = v2.y;
    mesh.vertices[index++] = v2.z;

    mesh.vertices[index++] = v4.x;
    mesh.vertices[index++] = v4.y;
    mesh.vertices[index++] = v4.z;

    // Third triangle
    mesh.vertices[index++] = v2.x;
    mesh.vertices[index++] = v2.y;
    mesh.vertices[index++] = v2.z;

    mesh.vertices[index++] = v3.x;
    mesh.vertices[index++] = v3.y;
    mesh.vertices[index++] = v3.z;

    mesh.vertices[index++] = v4.x;
    mesh.vertices[index++] = v4.y;
    mesh.vertices[index++] = v4.z;


    // Fourth triangle
    mesh.vertices[index++] = v1.x;
    mesh.vertices[index++] = v1.y;
    mesh.vertices[index++] = v1.z;

    mesh.vertices[index++] = v3.x;
    mesh.vertices[index++] = v3.y;
    mesh.vertices[index++] = v3.z;

    mesh.vertices[index++] = v4.x;
    mesh.vertices[index++] = v4.y;
    mesh.vertices[index++] = v4.z;
  }


  UploadMesh(&mesh, false);


  return mesh;
}


#define da_append(da, item) \
  if ((da)->count >= (da)->capacity) {  \
    if ((da)->capacity == 0) { \
      (da)->capacity = 1; \
    } \
    (da)->capacity *= 2;  \
    (da)->items = realloc((da)->items, (da)->capacity * sizeof(*(da)->items));  \
    assert((da)->items != NULL); \
  }  \
  (da)->items[(da)->count++] = item;  \

typedef struct Tri {
  Vector3 a;
  Vector3 b;
  Vector3 c;
  int a_index;
  int b_index;
  int c_index;
} Tri;

typedef struct TriDA {
  Tri *items;
  int count;
  int capacity;
} TriDA;


bool is_existing_triangle(TriDA *hull, Vector3 p1, Vector3 p2, Vector3 p3) {
  float eps = 0.0001f;
  for (int i = 0; i < hull->count; i++) {
    Tri tri = hull->items[i];

    bool is_tri = false;
    // 1
    bool p1_e_a = Vector3DistanceSqr(p1, tri.a) < eps;
    bool p2_e_b = Vector3DistanceSqr(p2, tri.b) < eps;
    bool p3_e_c = Vector3DistanceSqr(p3, tri.c) < eps;
    is_tri      = (p1_e_a && p2_e_b && p3_e_c);
    if (is_tri) return true;

    // 2
    p1_e_a      = Vector3DistanceSqr(p1, tri.a) < eps;
    bool p2_e_c = Vector3DistanceSqr(p2, tri.c) < eps;
    bool p3_e_b = Vector3DistanceSqr(p3, tri.b) < eps;
    is_tri      = (p1_e_a && p2_e_c && p3_e_b);
    if (is_tri) return true;

    // 3
    bool p1_e_b = Vector3DistanceSqr(p1, tri.b) < eps;
    bool p2_e_a = Vector3DistanceSqr(p2, tri.a) < eps;
    p3_e_c      = Vector3DistanceSqr(p3, tri.c) < eps;
    is_tri      = (p1_e_b && p2_e_a && p3_e_c);
    if (is_tri) return true;

    // 4
    p1_e_b      = Vector3DistanceSqr(p1, tri.b) < eps;
    p2_e_c      = Vector3DistanceSqr(p2, tri.c) < eps;
    bool p3_e_a = Vector3DistanceSqr(p3, tri.a) < eps;
    is_tri      = (p1_e_b && p2_e_c && p3_e_a);
    if (is_tri) return true;

    // 5
    bool p1_e_c = Vector3DistanceSqr(p1, tri.c) < eps;
    p2_e_a      = Vector3DistanceSqr(p2, tri.a) < eps;
    p3_e_b      = Vector3DistanceSqr(p3, tri.b) < eps;
    is_tri      = (p1_e_c && p2_e_a && p3_e_b);
    if (is_tri) return true;

    // 6
    p1_e_c      = Vector3DistanceSqr(p1, tri.c) < eps;
    p2_e_b      = Vector3DistanceSqr(p2, tri.b) < eps;
    p3_e_a      = Vector3DistanceSqr(p3, tri.a) < eps;
    is_tri      = (p1_e_c && p2_e_b && p3_e_a);
    if (is_tri) return true;
  }

  return false;
}


bool has_edge(Tri tri, Vector3 p1, Vector3 p2) {
  float eps = 0.00001f;
  bool is_first_edge = (
    (Vector3DistanceSqr(p1, tri.a) < eps && Vector3DistanceSqr(p2, tri.b) < eps)
    ||
    (Vector3DistanceSqr(p1, tri.b) < eps && Vector3DistanceSqr(p2, tri.a) < eps)
  );
  if (is_first_edge) return true;

  bool is_second_edge = (
    (Vector3DistanceSqr(p1, tri.b) < eps && Vector3DistanceSqr(p2, tri.c) < eps)
    ||
    (Vector3DistanceSqr(p1, tri.c) < eps && Vector3DistanceSqr(p2, tri.b) < eps)
  );
  if (is_second_edge) return true;

  bool is_third_edge = (
    (Vector3DistanceSqr(p1, tri.c) < eps && Vector3DistanceSqr(p2, tri.a) < eps)
    ||
    (Vector3DistanceSqr(p1, tri.a) < eps && Vector3DistanceSqr(p2, tri.c) < eps)
  );
  if (is_third_edge) return true;


  return false;
}


typedef struct EdgeKey {
  int edge_index_1;
  int edge_index_2;
} EdgeKey;

typedef struct {
  EdgeKey key; 
  int value; 
} EdgeKVP;

bool check_for_edge(EdgeKVP *edge_hm, int i1, int i2) {
  EdgeKey k = (EdgeKey) { i1, i2 };
  int index_1 = hmgeti(edge_hm, k);
  if (index_1 != -1) {
    return edge_hm[index_1].value > 1;
  }
  k = (EdgeKey) { i2, i1 };
  int index_2 = hmgeti(edge_hm, k);
  if (index_2 != -1){
    return edge_hm[index_2].value > 1;
  }

  return false;
}



typedef struct TriKey {
  int corner_index_1;
  int corner_index_2;
  int corner_index_3;
} TriKey;

typedef struct {
  TriKey key; 
  bool value; 
} TriKVP;

bool check_for_tri(TriKVP *tri_hm, int i1, int i2, int i3) {
  TriKey k;
  int index;

  k = (TriKey) { i1, i2, i3 };
  index = hmgeti(tri_hm, k);
  if (index != -1) return true;

  k = (TriKey) { i1, i3, i2};
  index = hmgeti(tri_hm, k);
  if (index != -1) return true;

  k = (TriKey) { i2, i1, i3};
  index = hmgeti(tri_hm, k);
  if (index != -1) return true;

  k = (TriKey) { i2, i3, i1};
  index = hmgeti(tri_hm, k);
  if (index != -1) return true;

  k = (TriKey) { i3, i1, i2};
  index = hmgeti(tri_hm, k);
  if (index != -1) return true;

  k = (TriKey) { i3, i2, i1};
  index = hmgeti(tri_hm, k);
  if (index != -1) return true;

  return false;
}

EdgeKVP *edge_hm = NULL;
TriKVP *tri_hm = NULL;

void expand_convex_hull(TriDA *hull, Vector3 *points, int n_points, int tri_index, int edge_index) {
  Tri tri = hull->items[tri_index];

  Vector3 p1, p2, p3;
  int i1, i2, i3;
  if (edge_index == 0) {
    p1 = tri.a;
    p2 = tri.b;
    p3 = tri.c;
    i1 = tri.a_index;
    i2 = tri.b_index;
    i3 = tri.c_index;
  } else if (edge_index == 1) {
    p1 = tri.b;
    p2 = tri.c;
    p3 = tri.a;
    i1 = tri.b_index;
    i2 = tri.c_index;
    i3 = tri.a_index;
  } else {
    p1 = tri.c;
    p2 = tri.a;
    p3 = tri.b;
    i1 = tri.c_index;
    i2 = tri.a_index;
    i3 = tri.b_index;
  }

  bool edge_exists = check_for_edge(edge_hm, i1, i2);
  if (edge_exists) return;
  // for (int i = 0; i < hull->count; i++) {
    // if (i == tri_index) continue;

    // bool edge_exists = has_edge(hull->items[i], p1, p2);
    // if (edge_exists) return;
  // }

  Vector3 normal = Vector3CrossProduct(
    Vector3Subtract(p1, p3),
    Vector3Subtract(p2, p3)
  );

  float min_cos_angle = 2.0f;
  int min_index = -1;
  for (int i = 0; i < n_points; i++) {
    if (i == i1 || i == i2 || i == i3) continue;
    Vector3 new_pt = points[i];
    // bool already_exists = is_existing_triangle(hull, p1, p2, new_pt);
    bool already_exists = check_for_tri(tri_hm, i1, i2, i); 
    if (already_exists) continue;



    Vector3 new_normal = Vector3CrossProduct(
      Vector3Subtract(p2, new_pt),
      Vector3Subtract(p1, new_pt)
    );

    float cos_angle = -Vector3DotProduct(normal, new_normal) / (Vector3Length(normal) * Vector3Length(new_normal));

    if (cos_angle < min_cos_angle) {
      min_cos_angle = cos_angle;
      min_index = i;
    }
  }
  assert(min_index != -1);
  assert(min_index != i1);
  assert(min_index != i2);

  Tri new_tri = (Tri) {
    points[min_index],
    p2,
    p1,
    i1,
    i2,
    min_index,
  };
  da_append(hull, new_tri);
  TriKey k = (TriKey) { i1, i2, min_index };
  hmput(tri_hm, k, true);

  EdgeKey ek = (EdgeKey) { i1, i2 };
  int edge_hm_index = hmgeti(edge_hm, ek);
  if (edge_hm_index == -1) {
    ek = (EdgeKey) { i2, i1 };
    edge_hm_index = hmgeti(edge_hm, ek);
  }

  int new_value;
  if (edge_hm_index == -1) {
    new_value = 1;
  } else {
    new_value = edge_hm[edge_hm_index].value + 1;
    if (new_value != 2) {
      printf("Edge value is %d\n", new_value);
    }
    assert(new_value == 2);
  }

  ek = (EdgeKey) { i1, i2 };
  hmput(edge_hm, ek, new_value);
}

Mesh calculate_convex_hull(Vector3 *points, int n_points) {
  // Find the initial triangle
  // Select 3 points with largest y-component (or x or z, doesn't matter)
  // Check that they are not on a line - If they are we cannot make a triangle
  // Otherwise, we are good to go
  Vector3 s1 =  points[0];
  Vector3 s2 =  points[0];
  Vector3 s3 =  points[0];
  int i1, i2, i3 = 0;
  // y1 >= y2 >= y3
  for (int i = 1; i < n_points; i++) {
    Vector3 pt = points[i];
    if (pt.y > s1.y) {
      s3 = s2;
      i3 = i2;

      s2 = s1;
      i2 = i1;

      s1 = pt;
      i1 = i;
    } else if (pt.y > s2.y) {
      s3 = s2;
      i3 = i2;

      s2 = pt;
      i2 = i;
    } else if (pt.y > s3.y) {
      s3 = pt;
      i3 = i;
    }
  }
  Vector3 d1 = Vector3Subtract(s2, s1);
  Vector3 d2 = Vector3Subtract(s3, s1);
  float dot_product = Vector3DotProduct(d1, d2);
  float cos_angle = dot_product / (Vector3Length(d1) * Vector3Length(d2));
  float eps = 0.0000001f;
  if (fabs(1.0 - cos_angle) < eps) {
    assert(0 && "Unable to construct initial triangle");
    // TODO: Handle failure better, e.g. try with x or z.
  }
  Tri initial_tri = (Tri) { s1, s2, s3, i1, i2, i3 };
  // printf("Starting convex hull from:\n");
  // printf("s1: %0.4f %0.4f %0.4f\n", s1.x, s1.y, s1.z);
  // printf("s2: %0.4f %0.4f %0.4f\n", s2.x, s2.y, s2.z);
  // printf("s3: %0.4f %0.4f %0.4f\n", s3.x, s3.y, s3.z);
  TriDA tris = {0};
  da_append(&tris, initial_tri);

  // Set up hashmaps
  edge_hm = NULL;
  EdgeKey k;

  k = (EdgeKey) { i1, i2 };
  hmput(edge_hm, k, 1);
  k = (EdgeKey) { i2, i3 };
  hmput(edge_hm, k, 1);
  k = (EdgeKey) { i3, i1 };
  hmput(edge_hm, k, 1);


  tri_hm = NULL;
  TriKey tk;
  tk = (TriKey) { i1, i2, i3 };
  hmput(tri_hm, tk, true);


  // Run the convex hull algorithm
  int tri_index = 0;
  int edge_index = 0;
  int iteration = 0;
  // printf("Starting convex hull calculation for %d points\n", n_points);
  while (tri_index < tris.count) {
    // printf("Iteration %d \n", iteration++);
    if (edge_index < 3) {
      expand_convex_hull(&tris, points, n_points, tri_index, edge_index);
      edge_index++;
    } else {
      edge_index = 0;
      tri_index++;
    }

    printf("Triangles: %d\r", tris.count);
    if (tris.count > 10000) break;
  } 
  // printf("Done. Final triangle count: %d\n", tris.count);

  // Turn the result of that algorithm into a Raylib Mesh
  Mesh mesh = {0};
  mesh.vertexCount = tris.count * 3;
  mesh.triangleCount = tris.count;
  // mesh.vertices = malloc(mesh.vertexCount * 3 * sizeof(float));
  mesh.vertices = calloc(mesh.vertexCount * 3, sizeof(float));
  int vertex_index = 0;
  for (int i = 0; i < tris.count; i++) {
    assert(Vector3Length(tris.items[i].a) < 2.0f);
    assert(vertex_index * 3 + 2 < mesh.vertexCount * 3);
    mesh.vertices[vertex_index * 3 + 0] = tris.items[i].a.x;
    mesh.vertices[vertex_index * 3 + 1] = tris.items[i].a.y;
    mesh.vertices[vertex_index * 3 + 2] = tris.items[i].a.z;
    vertex_index++;

    assert(Vector3Length(tris.items[i].b) < 2.0f);
    assert(vertex_index * 3 + 2 < mesh.vertexCount * 3);
    mesh.vertices[vertex_index * 3 + 0] = tris.items[i].b.x;
    mesh.vertices[vertex_index * 3 + 1] = tris.items[i].b.y;
    mesh.vertices[vertex_index * 3 + 2] = tris.items[i].b.z;
    vertex_index++;

    assert(Vector3Length(tris.items[i].c) < 2.0f);
    assert(vertex_index * 3 + 2 < mesh.vertexCount * 3);
    mesh.vertices[vertex_index * 3 + 0] = tris.items[i].c.x;
    mesh.vertices[vertex_index * 3 + 1] = tris.items[i].c.y;
    mesh.vertices[vertex_index * 3 + 2] = tris.items[i].c.z;
    vertex_index++;
  }

  UploadMesh(&mesh, false);

  return mesh;
}

PlyData subsample(PlyData data, int n) {
  int n_vertices = data.num_vertices / n;
  Vector3 *vertices = calloc(n_vertices, sizeof(Vector3));
  for (int i = 0; i < n_vertices; i++) {
    vertices[i] = data.vertices[i * n];
  }

  return (PlyData) {n_vertices, vertices};
}

int main(void) {
  PlyData raw_data = read_ply_file("bunny/reconstruction/bun_zipper.ply");
  PlyData data = subsample(raw_data, 10);


  InitWindow(800, 800, "3D Stuff");
  SetTargetFPS(60);
  DisableCursor();

  Mesh mesh = create_mesh_from_ply_data(data);
  Model model = LoadModelFromMesh(mesh);
  time_t t0 = time(NULL);
  Mesh mesh2 = calculate_convex_hull(data.vertices, data.num_vertices);
  time_t t1 = time(NULL);
  printf("Convex hull took %ld seconds\n", t1 - t0);
  Model model2 = LoadModelFromMesh(mesh2);

  Camera camera = {0};
  camera.position = (Vector3) {0.0f, 00.0f, 10.0f};
  camera.target = (Vector3) {0.0f, 0.0f, 0.0f};
  camera.up = (Vector3) {0.0f, 1.0f, 0.0f};
  camera.fovy = 45.0f;
  camera.projection = CAMERA_PERSPECTIVE;

  bool draw_convex_hull = false;
  while (!WindowShouldClose()) {
    BeginDrawing();
    ClearBackground(ColorFromHSV(0, 0, 0.2));

    BeginMode3D(camera);
    rlDisableBackfaceCulling();


    DrawModel(model, (Vector3) {0.0f, 0.0f, 0.0f}, 100.0f, WHITE);
    if (draw_convex_hull) {
      DrawModel(model2, (Vector3) {0.0f, 0.0f, 0.0f}, 100.0f, ColorAlpha(BLUE, 0.5));
      DrawModelWires(model2, (Vector3) {0.0f, 0.0f, 0.0f}, 100.0f, RED);
    }


    EndMode3D();
    DrawFPS(10, 10);
    EndDrawing();
    update_camera(&camera, CAMERA_FREE);


    if (IsKeyReleased(KEY_Z)) {
      camera.target = (Vector3) {0.0f, 0.0f, 0.0f};
    }
    if (IsKeyReleased(KEY_P)) {
      TakeScreenshot("screenshot.png");
    }
    if (IsKeyReleased(KEY_R)) {
      calculate_convex_hull(data.vertices, data.num_vertices);
    }
    if (IsKeyReleased(KEY_C)) {
      draw_convex_hull = !draw_convex_hull;
    }
  }


  UnloadModel(model);
  UnloadModel(model2);
  free_plydata(data);



  CloseWindow();
  return 0;
}
