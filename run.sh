#!/bin/bash
set -e

file="main.c"
gcc -o main $file -I/opt/raylib/src -L/opt/raylib/src -lraylib -lm -lpthread -ldl -ggdb
./main
